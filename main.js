const app = new Vue({
    el: "#app",
    data: {
      tempArray: [],
      uniqueLabels: [],
      festivals: []
    },
    mounted() {
      const headers = {
        method: 'GET',
        headers: {'Accept': 'application/json'}
      };
      //I couldn't get the JSON object due to CORS so i downloaded the data but I have constructed the call as if it's on a server.
      fetch("response_1624597384884.json", {headers}) 
        .then(response => response.json())
        .then((data) => {
          console.log(data)
          for (let i = 0; i < data.length; i++) {
            for (let j = 0; j < data[i].bands.length; j++) {
              if(data[i].bands[j].recordLabel) {
                //this will be use as reference to check for duplicates Record Label
                this.uniqueLabels.push(data[i].bands[j].recordLabel) 
                // re-construct the data to match requirement.
                this.tempArray.push({
                  'recordLabel': data[i].bands[j].recordLabel,
                  'name': data[i].bands[j].name,
                  'festivals': data[i].name
                });
              }
            }
          }

          this.uniqueLabels = [...new Set(this.uniqueLabels)]; //creating unique record labels, will be use to construct the new festivals array.
          
          //loop through array and grabbing their band and festival details base on unique record label and construct a new array (festivals)
          for (let i = 0; i < this.uniqueLabels.length; i++) {
            
            var getBandAndFestival = this.getBandAndFestivalAttended(this.uniqueLabels[i], this.tempArray);
            
            if(getBandAndFestival.recordLabel) {
              // console.log(getBandAndFestivalFromUnique2.recordLabel)
                this.festivals.push({
                  'recordLabel': getBandAndFestival.recordLabel,
                  'name': getBandAndFestival.band,
                  'festivals': getBandAndFestival.festival
                })
            }
          }
           console.log(this.festivals)
        })
    },
    methods: { //this method return all items association with their record label in an array.
      getBandAndFestivalAttended: function (item, theArray) {
        let band = [];
        let festival = [];
        let label = '';
          for (let i = 0; i < theArray.length; i++) {
            if(item == theArray[i].recordLabel) { 

              console.log('found: ' + item + " at " + i + " band: " + theArray[i].name);

              label = theArray[i].recordLabel;
              band.push(theArray[i].name);
              festival.push(theArray[i].festivals);
            }
          }

          //filter and clean out empty item before returning making it easier to display.
          festival = festival.filter(function (el){
            return el != null;
          })

          band = band.filter(function (el){
            return el != null;
          })

          return { 
            'recordLabel': label,
            'band': band, 
            'festival': festival,
          };
      }
    },
    computed: {
      sortRecordLabels: function () {
        function compare (a, b) {
          if (a.recordLabel < b.recordLabel)
            return -1;
          if (a.recordLabel > b.recordLabel)
            return 1;
          return 0
        }
        return this.festivals.sort(compare)
      }
    },

    template: `
    <div>
    <h3> Festival List </h3>

      <ul>
          <li v-for="(recordLabel, i) in sortRecordLabels">
            <h4>{{ recordLabel.recordLabel }}</h4>
            <ul class="recordLabel">
              <li v-for="band in recordLabel.name"> {{ band }} </li>
               <ul>
                <li v-for="festival in recordLabel.festivals"><i>{{ festival }} </i></li>
               </ul>
            </ul>
          </li>
      </ul>
    </div>
    `,
});